import Phaser from 'phaser'

class Avatar extends Phaser.GameObjects.Sprite {

  constructor({ 
      key, scene, x, y, animations, interactive = false,
      onClick : handleClick = e=>{}, onAnimComplete : handleAnimComplete = e=>{} 
    }){
    
    super( scene, x, y, 'sprites', key + '/idle/01.png' );

    this.handleAnimComplete = handleAnimComplete;

    this.key = key;

    this.animations = animations;

    this.setScale(1,1);

    this.buildAnimations();

    this.scene.add.existing(this);

    this.background_scene = scene.scene.get("BackgroundScene");

    this.on('animationcomplete', e =>{

      this.onAnimationComplete(e.key);
    });

    if(interactive)
    this.setInteractive({ useHandCursor: true })
    .on('pointerup', e => {
      handleClick( this.key );
    })
    .on('pointerover', e => {
      this.setScale(1.02,1.02); 
      // this.background_scene.playSound('click')
    })
    .on('pointerout', e => {
      this.setScale(1,1); 
    })
  }

  stopAnims(){
    clearTimeout(this.timeout);
  }

  onAnimationComplete( _key ){
    this.handleAnimComplete( _key );
    
    if( _key.indexOf("idle") == -1 ){

      clearTimeout( this.timeout );

      this.timeout = setTimeout( () => { this.playAnim( "idle" ) }, 600 );
    }
  }

  playAnim(_name){
    if( this.scene && this.scene.anims.get( this.key + "_" + _name ) ){
      clearTimeout(this.timeout);

      this.anims.play( this.key + "_" + _name );

      if( _name == "angry" ){
        this.background_scene.playSound( "effects-miss", {volume:.3} );
      }
    }
  }

  buildAnimations(){

    this.animations.forEach( _anim => {
      const _anim_key = this.key + '_' + _anim.key;

      if( !this.scene.anims.get( _anim_key ) ){
        const _frames = this.scene.anims.generateFrameNames( this.texture.key, {
          start: _anim.start || 1, 
          end: _anim.end || 5, 
          zeroPad: _anim.zeroPad || 2,
          prefix: this.key + '/' + _anim.key + '/', 
          suffix: '.png'
        } );

        this.scene.anims.create( { 
          key: _anim_key, 
          frames: _frames, 
          frameRate: _anim.frameRate || 8, 
          repeat: _anim.repeat != undefined ? _anim.repeat : -1,
          repeatDelay: _anim.repeatDelay || 0,
          delay: _anim.delay || 0 
        } );
      }
    });
  }
}

export default Avatar
