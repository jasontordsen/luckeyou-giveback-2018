class GameSession {
    constructor( _user = "" ){
        this.meals = 0;
        this.level = 1;
        this.score = 0;
        this.avatar = "";
        this.user = _user;
        this.first_play = true;

        console.log( "GameSession: ", this.user );
    }

    getemail(){
        return this.email;
    }

    reset(){
        this.level = 1;
        this.meals = 0;
        this.score = 0;
    }
}
 
export default GameSession;