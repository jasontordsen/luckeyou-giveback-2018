class MealPool extends Phaser.GameObjects.Group {
    constructor({scene, onMiss:handleMiss}) {
      super(scene, {
        defaultKey: 'gift',
        maxSize: 5,
        createCallback: function (meal) {
          this.scene.matter.add.gameObject(meal);
          meal.setBounce( .2 );
        },
        removeCallback: function (meal) {
            console.log('Removed', meal);
        }
      });

      this.handleMiss = handleMiss;
      this.spawnPositions = [130,280,430,580,730,880];
      this.scene.add.existing(this);
    }

    update(){
      this.children.iterate(function (meal) {
          if( meal.spawn_time && this.scene.time.now-meal.spawn_time > this.lifespan ){
            meal.spawn_time = null;
            this.initKillMeal(meal, false);
          }
      }.bind(this));
    }

    init( {level, lifespan, spawn_rate, max_size}, randomize = false ){
      this.randomize = randomize;
      this.maxSize = max_size;
      this.spawnPositions = this.shuffle(this.spawnPositions);

      this.level = level;
      this.lifespan = lifespan;

      this.meal_timer = this.scene.time.addEvent({
        delay: spawn_rate,
        loop: true,
        callback: this.addMeal.bind( this )
      });

      this.addMeal();
    }

    destroy(){
      if(this.meal_timer) this.meal_timer.destroy();

      if(this.children){
        this.children.iterate(function(meal){
          this.scene.tweens.killTweensOf(meal);
        }.bind( this ));
      }

      super.destroy(true);
    }

    checkHit( _x, _margin = 45 ){
      if(this.children){
        var _hit = false;

        this.children.iterate(function(meal){
          if( Math.abs( meal.x-_x ) < _margin && meal.active && meal.y > 280 ){
            _hit = true; this.initKillMeal( meal, true );
          }
        }.bind( this ));

        return _hit;
      }
    }

    initKillMeal( meal, _immediate ){
      if( _immediate ){
        this.killTween( meal );
        this.killMeal( meal );
      } else {
        this.scene.tweens.add( {
          alpha:.5,
          yoyo:true,
          repeat:4 - (this.level),
          duration: 300,
          targets: [meal],
          onComplete: e => {
            if(meal.active) this.handleMiss( this.level );
            this.killMeal( meal );
          }
        } );
      }
    }

    killMeal( meal ){ 
      var mealx = Math.round( meal.x/50 )*50;
      this.spawnPositions.push(mealx);
      
      this.killTween(meal);
      meal.setBody({bodyX:0,bodyY:0,bodyWidth:0, bodyHeight:0});
      this.killAndHide( meal );
    }

    killTween( meal ){
      this.scene.tweens.killTweensOf(meal); 
      meal.alpha = 1;
    }
    
    addMeal(){
        var meal = this.get( 
          this.randomize ? Phaser.Math.Between(130,880) : this.spawnPositions[0], -(50 + Math.random()*100) );
    
        if (!meal) return;  

        this.killTween(meal);
        this.spawnPositions.shift();

        meal.spawn_time = this.scene.time.now;
        
        meal.setBody({bodyX:20,bodyY:0,bodyWidth:80, bodyHeight:79})
        meal.setRotation( (Math.random()/40) - (Math.random()/20) );
        meal.setAlpha(1);
        meal.setActive(true);
        meal.setVisible(true);
    }

    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }
  }

  export default MealPool

