class DescriptionWithNumber extends Phaser.GameObjects.Container {

    constructor({scene, x, y, before_text, after_text, number}){
        let _style = 
        { 
            align:"left", 
            fontFamily:"century-gothic",
            shadow: {
                offsetX: 3,
                offsetY: 3,
                color: '#FFFFFF',
                blur: 0,
                stroke: false,
                fill: true
            }
        }

        let _before = new Phaser.GameObjects.Text( 
            scene, 0, 0, 
            before_text,
            {..._style, fill:"#000",fontSize: ismobile ? 28 : 18 }
        );

        let _number = new Phaser.GameObjects.Text( 
            scene, _before.x + _before.width, 0,
            number, 
            {..._style, fill:"#ca2400",fontSize:ismobile ? 36 : 28}
        );
        _number.y = -6;

        let _after = new Phaser.GameObjects.Text( 
            scene, _number.x + _number.width, 0,
            after_text,
            {..._style, fill:"#000",fontSize:ismobile ? 28 : 18}
        );

        super(scene, x, y, [_before, _number, _after]);

        this.number = _number;
        this.after = _after;

        scene.add.existing( this );
    }

    update(_number){
        this.number.text = _number;
        this.after.x = this.number.x + this.number.width;
    }
}
 
export default DescriptionWithNumber;