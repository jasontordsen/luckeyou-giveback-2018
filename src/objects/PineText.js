class PineText extends Phaser.GameObjects.Text {
    constructor({
        scene, 
        x = 0, 
        align = 'center', 
        y = 0, 
        text, 
        stroke = '#fff', 
        color = '#ca2400', 
        size = '88px', 
        padding = 0, 
        backgroundColor = null, 
        strokeSize = 8,
        shadowColor = '#000',
        shadowSize = 1,
        fontFamily = 'cheap-pine-sans',
        shadowBlur = 1 }) {
        super(scene, x, y, text, 
            {
                fill:color, 
                fontSize:size, 
                padding:padding,
                fontFamily: fontFamily,
                stroke:stroke,
                backgroundColor:backgroundColor,
                strokeThickness:strokeSize,
                shadow: {
                    offsetX: shadowSize,
                    offsetY: shadowSize,
                    color: shadowColor,
                    blur: shadowBlur,
                    stroke: true,
                    fill: false
                },
                align:align
            }
        );

        scene.add.existing(this);

        switch (align) {
            case 'right':
                this.x -= this.width;
                break;
            case 'center':
                this.x -= this.width/2;
                break;
            default:
                break;
        }
    }
  }

  export default PineText