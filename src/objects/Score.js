import Phaser from 'phaser'

class Score extends Phaser.GameObjects.Container {
    constructor({scene, x, y, text}) {

        let _label = new Phaser.GameObjects.Text(scene,4,-10,text,{
            fill:"#fff",
            fontSize:18, 
            fontFamily:"los-feliz",
            stroke:"#3c251d",
            strokeThickness:2,
            shadow: {
                offsetX: 2,
                offsetY: 2,
                color: '#372014',
                blur: 0,
                stroke: false,
                fill: true
            },
        });
        
        let _background = new Phaser.GameObjects.TileSprite(scene,0,0,60,50,"ui","wood_button/m.png");
        let _left = new Phaser.GameObjects.Image(scene,-_background.width/2,0,"ui","wood_button/l.png");
        let _right = new Phaser.GameObjects.Image(scene,_background.width/2,0,"ui","wood_button/r.png");
        let _ham  = new Phaser.GameObjects.Image(scene,-40,0,"ui","ham.png");

        super(scene, x, -y*2, [_left,_background,_right,_label, _ham]);
        
        scene.add.existing(this);

        this.startY = y;
        this.label = _label;
    }

    update(_score){
        this.label.text = _score;
    }

    show(){
        this.scene.tweens.add({
            targets:[this],
            y: this.startY,
            duration: 400,
            ease: Phaser.Math.Easing.Back.Out,
            delay:800
        });
      }
    
      hide(){
          this.scene.tweens.add({
              targets:[this],
              y: -this.startY*2,
              duration: 400,
              ease: Phaser.Math.Easing.Back.In,
              onComplete:this.handleOnStart
          });
      }
  }

  export default Score