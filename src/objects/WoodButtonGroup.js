import WoodButton from "./WoodButton";

class WoodButtonGroup extends Phaser.GameObjects.Container {

    constructor({scene, x, y, buttons, onClick:handleClick}){
        let _children = [];

        for (let i = 0; i < buttons.length; i++) {
            const _button = buttons[i];
            const _prev_button = i > 0 ? _children[i-1] : null;
            const _x = _prev_button ? _prev_button.x + (_prev_button.getBounds().width/2) + 10 : 0;

            const _wood_button = new WoodButton({
                scene:scene,
                x:_x,
                y:0,
                text:_button.text,
                key:_button.key,
                onClick:handleClick
            });

            _wood_button.x += _wood_button.getBounds().width/2;

            _children.push( _wood_button );
        };

        super(scene, x, y, _children);

        this.buttons = _children;

        scene.add.existing(this);
    }
}
 
export default WoodButtonGroup;