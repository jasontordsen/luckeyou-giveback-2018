class Award extends Phaser.GameObjects.Container {

    constructor({scene}){

        let _badge = new Phaser.GameObjects.Image(scene,750,100,"ui","badge.png");
        _badge.setScale(0);

        let _bonus_points = new Phaser.GameObjects.Text( 
            scene, _badge.x + 50, _badge.y - 30, "+100",
            { 
                align:"left", 
                fontFamily:"century-gothic",
                shadow: {
                    offsetX: 3,
                    offsetY: 3,
                    color: '#FFFFFF',
                    blur: 0,
                    stroke: false,
                    fill: true
                }, 
                fill:"#ca2400",
                fontSize:24 
            }
        );

        _bonus_points.setScale(0);

        super(scene, 0, 0, [_bonus_points, _badge]);

        this.bonus_points = _bonus_points;
        this.badge = _badge;

        scene.add.existing( this );
    }

    show(_callback){
        var timeline = this.scene.tweens.createTimeline();

        timeline.add({
            targets:[this.badge],
            scaleX:1, 
            scaleY:1,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });
    
        timeline.add({
            targets: this.bonus_points,
            scaleX:1, 
            scaleY:1,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
            onComplete:_callback
        });
    
        timeline.play();
    }
}
 
export default Award;