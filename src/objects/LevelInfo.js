import PineText from "./PineText";
import DescriptionWithNumber from "./DescriptionWithNumber";

class LevelInfo extends Phaser.GameObjects.Container {

    constructor({scene, conf, x, onStart:handleOnStart}){
        let _heading = new PineText({
            scene:scene, text:"LEVEL " + conf.id, y:0, align:"right", x:0, color:"#22BB22", size:96
        });
        _heading.x = -_heading.width/2;

        let _description = new DescriptionWithNumber(
            {
                scene:scene, 
                x:0,
                y:_heading.y + _heading.height + 8,
                before_text:"Grab ",
                after_text:" meals",
                number:conf.goal
            }
        );

        let _description_bounds = _description.getBounds();

        _description.x = -_description_bounds.width/2;

        super(scene, x, -100, [_heading, _description]);

        scene.add.existing( this );
    }

    show(){
        this.scene.tweens.add({
            targets:[this],
            y: 50,
            alpha:1,
            duration: 400,
            ease: Phaser.Math.Easing.Back.Out,
            delay:400
        });
    }

    hide(){
        this.scene.tweens.add({
            targets:[this],
            y: -200,
            alpha:0,
            duration: 400,
            ease: Phaser.Math.Easing.Back.In,
            onComplete:this.handleOnStart
        });
    }
}
 
export default LevelInfo;