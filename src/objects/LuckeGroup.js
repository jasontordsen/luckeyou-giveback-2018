import Avatar from '../sprites/Avatar'
import config from '../config'
import PineButton from './PineButton';

class LuckeGroup extends Phaser.GameObjects.Container {
    
    constructor({scene, start_anim = "idle", leader:_leader, interactive = false, onClick:handleClick = function(){} }) {

      let jason = new Avatar(
        {
          key:'jason',
          scene: scene, 
          x: 170, y:0,
          animations: config.avatars.animations.jason,
          interactive: interactive,
          onClick : handleClick
        }
      )

      let tim = new Avatar(
        {
          key:'tim',
          scene: scene, 
          x: 410, y:0,
          animations: config.avatars.animations.tim,
          interactive: interactive,
          onClick : handleClick
        }
      )
      
      let nick = new Avatar(
        {
          key:'nick',
          scene: scene, 
          x: 625, y:0,
          animations: config.avatars.animations.nick,
          interactive: interactive,
          onClick : handleClick
        }
      )
        
      let manny = new Avatar(
        {
          key:'manny',
          scene: scene, 
          x: 850, y:0,
          animations: config.avatars.animations.manny,
          interactive: interactive,
          onClick : handleClick
        }
      )

      super(scene, 0, 810, [jason, tim, nick, manny]);

      this.jason = jason;
      this.tim = tim;
      this.manny = manny;
      this.nick = nick;

      this.leader = _leader;

      scene.add.existing( this );

      this.transitionIn( start_anim );
    }

    transitionIn( _anim ){
      var timeline = this.scene.tweens.createTimeline();

        timeline.add({
            targets:this.jason,
            ...config.avatars.transitions.in,
            onStart:e=>{
              this.scene.background_scene.playSound('effects-woosh', {volume:.2, rate:1});
            }
        });

        timeline.add({
            targets:this.tim,
            ...config.avatars.transitions.in,
            onStart:e=>{
              this.scene.background_scene.playSound('effects-woosh', {volume:.21, rate:1.1});
            }
        });

        timeline.add({
            targets:this.nick,
            ...config.avatars.transitions.in,
            onStart:e=>{
              this.scene.background_scene.playSound('effects-woosh', {volume:.22, rate:1.2});
            }
        });

        timeline.add({
            targets:this.manny,
            ...config.avatars.transitions.in,
            onStart:e=>{
              this.scene.background_scene.playSound('effects-woosh', {volume:.23, rate:1.3});
            },
            onComplete: e =>{
              this.playAnim( _anim )
            }
        });

        timeline.play();
    }

    stopAnims(){
      this.iterate( child => {
        if(child.stopAnims) child.stopAnims();
      });
    }

    playAnim( _key ){
      this.iterate( child => {
        var _anim = _key;

        if( this.leader && child.key == this.leader ){
          if( _key == "win-group" ) _anim = "win";
          if( _key == "lose-group" ) _anim = "lose";
        }

        if(child.playAnim) child.playAnim( _anim );
      });
    }
  }

  export default LuckeGroup