import Phaser from 'phaser'

class TextButton extends Phaser.GameObjects.Text {
    constructor(scene, x, y, text, handleClick) {
      super(scene, x, y, text, {fill:"#000"});
  
      this.background_scene = this.scene.scene.get("BackgroundScene");

      this.setInteractive({ useHandCursor: true })
        .on('pointerover', () => this.enterButtonHoverState() )
        .on('pointerout', () => this.enterButtonRestState() )
        .on('pointerdown', () => this.enterButtonActiveState() )
        .on('pointerup', () => {
            this.enterButtonHoverState();
            this.background_scene.playSound("click");
            handleClick();
        });
    }
  
    enterButtonHoverState() {
      this.setStyle({ fill: 'red'});
      this.background_scene.playSound('click')
    }
  
    enterButtonRestState() {
      this.setStyle({ fill: '#000'});
    }
  
    enterButtonActiveState() {
      this.setStyle({ fill: 'blue' });
    }
  }

  export default TextButton