import PineText from './PineText'

class PineButton extends PineText {
    constructor( params ){
      let {onClick:handleClick} = params;

      super( params );
  
      this.background_scene = this.scene.scene.get("BackgroundScene");

      this.setInteractive({ useHandCursor: true })
        .on('pointerover', () => this.enterButtonHoverState() )
        .on('pointerout', () => this.enterButtonRestState() )
        .on('pointerdown', () => this.enterButtonActiveState() )
        .on('pointerup', () => {
            this.enterButtonHoverState();
            this.background_scene.playSound("effects-click");
            handleClick();
        });
    }
  
    enterButtonHoverState() {
      this.scene.tweens.killTweensOf(this);
      this.setScale()
      this.setAlpha(1);
      this.setStyle({ fill: '#22BB22'});
    }
  
    enterButtonRestState() {
      this.setAlpha(1);
      this.setStyle({ fill: '#FF4444'});
    }
  
    enterButtonActiveState() {
      this.setAlpha(1);
      this.setStyle({ fill: '#0055CC' });
    }
  }

  export default PineButton