import Phaser from 'phaser'

class WoodButton extends Phaser.GameObjects.Container {
    constructor({scene, x, y, text, key, onClick:handleClick}) {

        let _label = new Phaser.GameObjects.Text(scene,0,0,text,{
            fill:"#FFFFCC",
            fontSize:18, 
            fontFamily:"los-feliz",
            stroke:"#3c251d",
            strokeThickness:2,
            shadow: {
                offsetX: 2,
                offsetY: 2,
                color: '#372014',
                blur: 0,
                stroke: false,
                fill: true
            },
        });
        
        _label.x = -_label.width/2;
        _label.y = 2-(_label.height/2);
        
        let _background = new Phaser.GameObjects.TileSprite(scene,0,0,_label.width+20,50,"ui","wood_button/m.png");

        let _left = new Phaser.GameObjects.Image(scene,-_background.width/2,0,"ui","wood_button/l.png");

        let _right = new Phaser.GameObjects.Image(scene,_background.width/2,0,"ui","wood_button/r.png");

        super(scene, x, y, [_left,_background,_right,_label]);

        this.background_scene = this.scene.scene.get("BackgroundScene");

        scene.add.existing(this);
        
        this.label = _label;
        this.background = _background;

        _background.setInteractive({ useHandCursor: true })
        .on('pointerover', () => this.enterButtonHoverState() )
        .on('pointerout', () => this.enterButtonRestState() )
        .on('pointerdown', () => this.enterButtonActiveState() )
        .on('pointerup', () => {
            this.enterButtonHoverState();
            this.background_scene.playSound("effects-click");
            handleClick( key );
        });
        this.setScale(ismobile ? 1.3 : 1);
    }

    getWidth(){
        return this.background.width;
    }
  
    enterButtonHoverState() {
        this.label.setStyle({ fill: '#FFF'});
        this.setScale(ismobile ? 1.35 : 1.05);
        this.setAlpha(1);
        this.scene.background_scene.playSound('effects-woosh', {volume:.12, rate:2});
    }
  
    enterButtonRestState() {
        this.label.setStyle({ fill: '#FFFFCC'});
        this.setScale(1);
    }
  
    enterButtonActiveState() {
        this.label.setStyle({ fill: '#FFF' });
        this.setScale(ismobile ? 1.35 : 1.05);
    }
  }

  export default WoodButton