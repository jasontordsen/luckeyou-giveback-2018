class ProgressBar extends Phaser.GameObjects.Container {
  constructor({scene, x, y, width, level}) {
    
    let _box = new Phaser.GameObjects.Rectangle(scene,55,2,width-50,20, 0x422a23);
    let _bar = new Phaser.GameObjects.Rectangle(scene,55,2,width-54,16, 0x22BB22);
    let _background = new Phaser.GameObjects.TileSprite(scene,0,0,_box.width+80,50,"ui","wood_button/m.png");
    let _left = new Phaser.GameObjects.Image(scene,-_background.width/2,0,"ui","wood_button/l.png");
    let _right = new Phaser.GameObjects.Image(scene,_background.width/2,0,"ui","wood_button/r.png");
    let _clock  = new Phaser.GameObjects.Image(scene,width/2,0,"ui","clock.png");

    let _label = new Phaser.GameObjects.Text(scene,-(width/2)-6,-10,"LEVEL " + level,{
        fill:"#fff",
        fontSize:18, 
        fontFamily:"los-feliz",
        stroke:"#3c251d",
        strokeThickness:2,
        shadow: {
            offsetX: 2,
            offsetY: 2,
            color: '#372014',
            blur: 0,
            stroke: false,
            fill: true
        },
    });
  
    super(scene, x, -y*2, [_background,_left,_right,_box, _bar, _clock, _label]);

    this.label = _label;

    scene.add.existing(this);

    this.startY = y;

    this.bar = _bar;
    this.bar_width = this.bar.width;

    this.update(0);
  }

  stop(){
      this.scene.tweens.killTweensOf(this.bar);
      this.scene.background_scene.stopSound('effects-clock');
  }

  warn(){
    
    if(!this.scene.tweens.isTweening(this.bar)){
        this.bar.fillColor = 0xca2400;
        this.scene.tweens.add({
            targets:this.bar,
            repeat:-1,
            yoyo:true,
            alpha:.5,
            duration:400
        });
        this.scene.background_scene.playSound('effects-clock', {rate:1.2, volume:.2, loop:true}, true);
    }
  }

  update(_progress) {
    this.bar.width = this.bar_width - (this.bar_width * _progress);
  }

  show(){
    this.scene.tweens.add({
        targets:[this],
        y: this.startY,
        duration: 400,
        ease: Phaser.Math.Easing.Back.Out,
        delay:400
    });
  }

  hide(){
      this.scene.tweens.add({
          targets:[this],
          y: -this.startY*2,
          duration: 400,
          ease: Phaser.Math.Easing.Back.In,
          onComplete:this.handleOnStart
      });
  }
}

export default ProgressBar;
