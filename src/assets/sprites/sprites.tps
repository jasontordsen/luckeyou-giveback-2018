<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.10.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser</string>
        <key>textureFileName</key>
        <filename>../../../assets/sprites/sprites_{n}.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>json</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../assets/sprites/sprites.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,1.215</point_f>
            <key>writePivotPoints</key>
            <true/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">jason/angry/01.png</key>
            <key type="filename">jason/angry/02.png</key>
            <key type="filename">jason/angry/03.png</key>
            <key type="filename">jason/angry/04.png</key>
            <key type="filename">jason/angry/05.png</key>
            <key type="filename">jason/angry/06.png</key>
            <key type="filename">jason/angry/07.png</key>
            <key type="filename">jason/angry/08.png</key>
            <key type="filename">jason/angry/09.png</key>
            <key type="filename">jason/angry/10.png</key>
            <key type="filename">jason/angry/11.png</key>
            <key type="filename">jason/attract/01.png</key>
            <key type="filename">jason/attract/02.png</key>
            <key type="filename">jason/attract/03.png</key>
            <key type="filename">jason/attract/04.png</key>
            <key type="filename">jason/attract/05.png</key>
            <key type="filename">jason/attract/06.png</key>
            <key type="filename">jason/attract/07.png</key>
            <key type="filename">jason/attract/08.png</key>
            <key type="filename">jason/gift/01.png</key>
            <key type="filename">jason/gift/02.png</key>
            <key type="filename">jason/gift/03.png</key>
            <key type="filename">jason/gift/04.png</key>
            <key type="filename">jason/gift/05.png</key>
            <key type="filename">jason/gift/06.png</key>
            <key type="filename">jason/idle/01.png</key>
            <key type="filename">jason/idle/02.png</key>
            <key type="filename">jason/idle/03.png</key>
            <key type="filename">jason/idle/04.png</key>
            <key type="filename">jason/idle/05.png</key>
            <key type="filename">jason/lose-group/01.png</key>
            <key type="filename">jason/lose-group/02.png</key>
            <key type="filename">jason/lose-group/03.png</key>
            <key type="filename">jason/lose-group/04.png</key>
            <key type="filename">jason/lose-group/05.png</key>
            <key type="filename">jason/lose-group/06.png</key>
            <key type="filename">jason/lose/01.png</key>
            <key type="filename">jason/lose/02.png</key>
            <key type="filename">jason/lose/03.png</key>
            <key type="filename">jason/win-group/01.png</key>
            <key type="filename">jason/win-group/02.png</key>
            <key type="filename">jason/win-group/03.png</key>
            <key type="filename">jason/win-group/04.png</key>
            <key type="filename">jason/win/01.png</key>
            <key type="filename">jason/win/02.png</key>
            <key type="filename">jason/win/03.png</key>
            <key type="filename">jason/win/04.png</key>
            <key type="filename">jason/win/05.png</key>
            <key type="filename">jason/win/06.png</key>
            <key type="filename">jason/win/07.png</key>
            <key type="filename">jason/win/08.png</key>
            <key type="filename">jason/win/09.png</key>
            <key type="filename">jason/win/10.png</key>
            <key type="filename">jason/win/11.png</key>
            <key type="filename">jason/win/12.png</key>
            <key type="filename">jason/win/13.png</key>
            <key type="filename">manny/angry/01.png</key>
            <key type="filename">manny/angry/02.png</key>
            <key type="filename">manny/angry/03.png</key>
            <key type="filename">manny/angry/04.png</key>
            <key type="filename">manny/angry/05.png</key>
            <key type="filename">manny/angry/06.png</key>
            <key type="filename">manny/angry/07.png</key>
            <key type="filename">manny/angry/08.png</key>
            <key type="filename">manny/angry/09.png</key>
            <key type="filename">manny/angry/10.png</key>
            <key type="filename">manny/attract/01.png</key>
            <key type="filename">manny/attract/02.png</key>
            <key type="filename">manny/attract/03.png</key>
            <key type="filename">manny/attract/04.png</key>
            <key type="filename">manny/attract/05.png</key>
            <key type="filename">manny/attract/06.png</key>
            <key type="filename">manny/attract/07.png</key>
            <key type="filename">manny/attract/08.png</key>
            <key type="filename">manny/attract/09.png</key>
            <key type="filename">manny/attract/10.png</key>
            <key type="filename">manny/attract/11.png</key>
            <key type="filename">manny/attract/12.png</key>
            <key type="filename">manny/attract/13.png</key>
            <key type="filename">manny/attract/14.png</key>
            <key type="filename">manny/gift/01.png</key>
            <key type="filename">manny/gift/02.png</key>
            <key type="filename">manny/gift/03.png</key>
            <key type="filename">manny/gift/04.png</key>
            <key type="filename">manny/gift/05.png</key>
            <key type="filename">manny/gift/06.png</key>
            <key type="filename">manny/idle/01.png</key>
            <key type="filename">manny/idle/02.png</key>
            <key type="filename">manny/idle/03.png</key>
            <key type="filename">manny/idle/04.png</key>
            <key type="filename">manny/idle/05.png</key>
            <key type="filename">manny/lose-group/01.png</key>
            <key type="filename">manny/lose-group/02.png</key>
            <key type="filename">manny/lose-group/03.png</key>
            <key type="filename">manny/lose-group/04.png</key>
            <key type="filename">manny/lose-group/05.png</key>
            <key type="filename">manny/lose-group/06.png</key>
            <key type="filename">manny/lose/01.png</key>
            <key type="filename">manny/lose/02.png</key>
            <key type="filename">manny/lose/03.png</key>
            <key type="filename">manny/win-group/01.png</key>
            <key type="filename">manny/win-group/02.png</key>
            <key type="filename">manny/win-group/03.png</key>
            <key type="filename">manny/win-group/04.png</key>
            <key type="filename">manny/win/01.png</key>
            <key type="filename">manny/win/02.png</key>
            <key type="filename">manny/win/03.png</key>
            <key type="filename">manny/win/04.png</key>
            <key type="filename">manny/win/05.png</key>
            <key type="filename">manny/win/06.png</key>
            <key type="filename">nick/angry/01.png</key>
            <key type="filename">nick/angry/02.png</key>
            <key type="filename">nick/angry/03.png</key>
            <key type="filename">nick/angry/04.png</key>
            <key type="filename">nick/angry/05.png</key>
            <key type="filename">nick/angry/06.png</key>
            <key type="filename">nick/angry/07.png</key>
            <key type="filename">nick/angry/08.png</key>
            <key type="filename">nick/attract/01.png</key>
            <key type="filename">nick/attract/02.png</key>
            <key type="filename">nick/attract/03.png</key>
            <key type="filename">nick/attract/04.png</key>
            <key type="filename">nick/attract/05.png</key>
            <key type="filename">nick/attract/06.png</key>
            <key type="filename">nick/attract/07.png</key>
            <key type="filename">nick/attract/08.png</key>
            <key type="filename">nick/attract/09.png</key>
            <key type="filename">nick/attract/10.png</key>
            <key type="filename">nick/gift/01.png</key>
            <key type="filename">nick/gift/02.png</key>
            <key type="filename">nick/gift/03.png</key>
            <key type="filename">nick/gift/04.png</key>
            <key type="filename">nick/gift/05.png</key>
            <key type="filename">nick/gift/06.png</key>
            <key type="filename">nick/idle/01.png</key>
            <key type="filename">nick/idle/02.png</key>
            <key type="filename">nick/idle/03.png</key>
            <key type="filename">nick/idle/04.png</key>
            <key type="filename">nick/idle/05.png</key>
            <key type="filename">nick/lose-group/01.png</key>
            <key type="filename">nick/lose-group/02.png</key>
            <key type="filename">nick/lose-group/03.png</key>
            <key type="filename">nick/lose-group/04.png</key>
            <key type="filename">nick/lose-group/05.png</key>
            <key type="filename">nick/lose-group/06.png</key>
            <key type="filename">nick/lose/01.png</key>
            <key type="filename">nick/lose/02.png</key>
            <key type="filename">nick/lose/03.png</key>
            <key type="filename">nick/win-group/01.png</key>
            <key type="filename">nick/win-group/02.png</key>
            <key type="filename">nick/win-group/03.png</key>
            <key type="filename">nick/win-group/04.png</key>
            <key type="filename">nick/win/01.png</key>
            <key type="filename">nick/win/02.png</key>
            <key type="filename">nick/win/03.png</key>
            <key type="filename">nick/win/04.png</key>
            <key type="filename">nick/win/05.png</key>
            <key type="filename">nick/win/06.png</key>
            <key type="filename">nick/win/07.png</key>
            <key type="filename">nick/win/08.png</key>
            <key type="filename">nick/win/09.png</key>
            <key type="filename">tim/angry/01.png</key>
            <key type="filename">tim/angry/02.png</key>
            <key type="filename">tim/angry/03.png</key>
            <key type="filename">tim/angry/04.png</key>
            <key type="filename">tim/angry/05.png</key>
            <key type="filename">tim/angry/06.png</key>
            <key type="filename">tim/angry/07.png</key>
            <key type="filename">tim/angry/08.png</key>
            <key type="filename">tim/attract/01.png</key>
            <key type="filename">tim/attract/02.png</key>
            <key type="filename">tim/attract/03.png</key>
            <key type="filename">tim/attract/04.png</key>
            <key type="filename">tim/attract/05.png</key>
            <key type="filename">tim/attract/06.png</key>
            <key type="filename">tim/attract/07.png</key>
            <key type="filename">tim/attract/08.png</key>
            <key type="filename">tim/attract/09.png</key>
            <key type="filename">tim/attract/10.png</key>
            <key type="filename">tim/attract/11.png</key>
            <key type="filename">tim/attract/12.png</key>
            <key type="filename">tim/gift/01.png</key>
            <key type="filename">tim/gift/02.png</key>
            <key type="filename">tim/gift/03.png</key>
            <key type="filename">tim/gift/04.png</key>
            <key type="filename">tim/gift/05.png</key>
            <key type="filename">tim/idle/01.png</key>
            <key type="filename">tim/idle/02.png</key>
            <key type="filename">tim/idle/03.png</key>
            <key type="filename">tim/idle/04.png</key>
            <key type="filename">tim/idle/05.png</key>
            <key type="filename">tim/lose-group/01.png</key>
            <key type="filename">tim/lose-group/02.png</key>
            <key type="filename">tim/lose-group/03.png</key>
            <key type="filename">tim/lose-group/04.png</key>
            <key type="filename">tim/lose-group/05.png</key>
            <key type="filename">tim/lose-group/06.png</key>
            <key type="filename">tim/lose/01.png</key>
            <key type="filename">tim/lose/02.png</key>
            <key type="filename">tim/lose/03.png</key>
            <key type="filename">tim/win-group/01.png</key>
            <key type="filename">tim/win-group/02.png</key>
            <key type="filename">tim/win-group/03.png</key>
            <key type="filename">tim/win-group/04.png</key>
            <key type="filename">tim/win/01.png</key>
            <key type="filename">tim/win/02.png</key>
            <key type="filename">tim/win/03.png</key>
            <key type="filename">tim/win/04.png</key>
            <key type="filename">tim/win/05.png</key>
            <key type="filename">tim/win/06.png</key>
            <key type="filename">tim/win/07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.899</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>150,125,300,250</rect>
                <key>scale9Paddings</key>
                <rect>150,125,300,250</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tim/gift/06.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.899</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>0,0,0,0</rect>
                <key>scale9Paddings</key>
                <rect>0,0,0,0</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array>
            <string>phaser3-exporter-beta</string>
        </array>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
