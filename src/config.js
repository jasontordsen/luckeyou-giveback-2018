import Phaser from "phaser";

export default {
  type: Phaser.WEBGL,
  parent: "content",
  width: 1024,
  height: 600,
  localStorageName: "phaseres6webpack",
  render: { transparent: true },
  physics: {
    debug: true,
    default: "matter",
    matter: {}
  },
  game: {
    start_level: 0,
    bounds: [100, 924],
    progress_bar_width: 400,
    levels:[
      {
        id:"1",
        goal:5,
        avatar_speed:40,
        miss_factor:2.5,
        death_factor:3,
        lifespan:1500,
        max_pool_size:2,
        spawn_rate:2000,
        duration: 14000
      },
      {
        id:"2",
        goal:6,
        avatar_speed:40,
        miss_factor:2.5,
        death_factor:3,
        lifespan:1700,
        max_pool_size:4,
        spawn_rate:1800,
        duration: 14000
      },
      {
        id:"3",
        goal:7,
        avatar_speed:40,
        miss_factor:2.5,
        death_factor:3,
        lifespan:1600,
        max_pool_size:4,
        spawn_rate:1600,
        duration: 14000
      },
      {
        id:"4",
        goal:8,
        avatar_speed:45,
        miss_factor:2,
        death_factor:2.5,
        lifespan:1500,
        max_pool_size:4,
        spawn_rate:1500,
        duration: 14000
      },
      {
        id:"5",
        goal:9,
        avatar_speed:45,
        miss_factor:2,
        death_factor:2.5,
        lifespan:1500,
        max_pool_size:5,
        spawn_rate:1400,
        duration: 15000
      },
      {
        id:"6",
        goal:10,
        avatar_speed:45,
        miss_factor:1.5,
        death_factor:2,
        lifespan:1400,
        max_pool_size:5,
        spawn_rate:1300,
        duration: 15000
      },
      {
        id:"7",
        goal:12,
        avatar_speed:50,
        miss_factor:1.3,
        death_factor:1.5,
        lifespan:1400,
        max_pool_size:5,
        spawn_rate:1200,
        duration: 15000
      },
      {
        id:"8",
        goal:13,
        avatar_speed:50,
        miss_factor:1.1,
        death_factor:1.2,
        lifespan:1300,
        max_pool_size:6,
        spawn_rate:1100,
        duration: 16000
      },
      {
        id:"9",
        goal:14,
        avatar_speed:55,
        miss_factor:1.05,
        death_factor:1.1,
        lifespan:1300,
        max_pool_size:6,
        spawn_rate:1000,
        duration: 16000
      },
      {
        id:"10",
        goal:16,
        avatar_speed:60,
        miss_factor:1.05,
        death_factor:1.1,
        lifespan:1200,
        max_pool_size:6,
        spawn_rate:900,
        duration: 16000
      }
    ]
  },
  avatars: {
    transitions: {
      in: {
        y: "-=400",
        duration: 400,
        ease: "Cubic",
        easeParams: [0.5],
      },
      out: {
        y: "+=400",
        duration: 1000,
        ease: "Cubic",
        easeParams: [0.5],
        delay: 400
      }
    },
    animations: {
      jason: [
        { key: "angry", end: 11, repeat: 0 },
        { key: "attract", end: 8, repeat: 1, delay: 2000 },
        { key: "gift", end: 6, repeat: 0, frameRate: 12 },
        { key: "idle", end: 5, repeatDelay: 1000 + Math.random() * 1500 },
        { key: "lose", end: 3, repeat: 5 },
        { key: "lose-group", end: 6, repeat: 5 },
        { key: "win", end: 13, repeat: 2 },
        { key: "win-group", end: 4, repeat: 2 }
      ],
      manny: [
        { key: "angry", end: 10, repeat: 0 },
        { key: "attract", end: 14, repeat: 0 },
        { key: "gift", end: 6, repeat: 0, frameRate: 12 },
        { key: "idle", end: 5, repeatDelay: 1000 + Math.random() * 1500 },
        { key: "lose", end: 3, repeat: 6 },
        { key: "lose-group", end: 6, repeat: 6 },
        { key: "win", end: 6, repeat: 4 },
        { key: "win-group", end: 4, repeat: 4 }
      ],
      nick: [
        { key: "angry", end: 8, repeat: 0 },
        { key: "attract", end: 10, repeat: 1, repeatDelay: 4000, delay: 1000 },
        { key: "gift", end: 6, repeat: 0, frameRate: 12 },
        { key: "idle", end: 5, repeatDelay: 1000 + Math.random() * 1500 },
        { key: "lose", end: 3, repeat: 3 },
        { key: "lose-group", end: 6, repeat: 3 },
        { key: "win", end: 9, repeat: 2, repeatDelay:2000 },
        { key: "win-group", end: 4, repeat: 3 }
      ],
      tim: [
        { key: "angry", end: 8, repeat: 0 },
        { key: "attract", end: 12, repeat: 0 },
        { key: "gift", end: 6, repeat: 0, frameRate: 12 },
        { key: "idle", end: 5, repeatDelay: 1000 + Math.random() * 1500 },
        { key: "lose", end: 3, repeat: 3 },
        { key: "lose-group", end: 6, repeat: 3 },
        { key: "win", end: 7, repeat: 5 },
        { key: "win-group", end: 4, repeat: 5 }
      ],
      
    }
  }
};
