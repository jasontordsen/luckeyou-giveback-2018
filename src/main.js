import Phaser from 'phaser'

import BootScene from './scenes/Boot'
import IntroScene from './scenes/Intro'
import DoneScene from './scenes/Done'
import StartScene from './scenes/Start'
import BackgroundScene from './scenes/Background'
import ForegroundScene from './scenes/Foreground'
import GameScene from './scenes/Game'
import LevelWinScene from './scenes/LevelWin'
import LevelLoseScene from './scenes/LevelLose'
import GameWinScene from './scenes/GameWin'
import config from './config'
import GameSession from './objects/GameSession';
import InstructionsScene from './scenes/Instructions';


const gameConfig = Object.assign(config, {
  scene: [BootScene, BackgroundScene, IntroScene, StartScene, GameScene, InstructionsScene, LevelWinScene, LevelLoseScene, GameWinScene, DoneScene, ForegroundScene ]
})

class Game extends Phaser.Game {
  constructor () {
    super( gameConfig )

    var urlvars = {};
    window.parent.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        urlvars[key] = value;
    });

    this.session = new GameSession( urlvars["u"] );
  }
}

window.game = new Game()