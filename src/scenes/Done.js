/* globals __DEV__ */
import Phaser from 'phaser'
import PineText from '../objects/PineText'
import config from "../config";
import WoodButtonGroup from '../objects/WoodButtonGroup';
import MealPool from "../objects/MealPool";

class DoneScene extends Phaser.Scene {

    constructor( config ) {
        super( { ...config, 'key': 'DoneScene' } );
    }

    init(){
        this.background_scene = this.scene.get('BackgroundScene');

        this.matter.world.setBounds( -30, 0, config.width + 60, 470, 90, false, false, false, true );
    }
  
    create() {
        this.meal_pool = new MealPool({
            scene: this
        });
          
        let _heading_top = new PineText({
            y:-20, x:config.width/2, scene:this, align:"center", text:"THANK YOU FOR HELPING US PROVIDE", size:40
        });
        _heading_top.setAlpha(0);

        let _number = new PineText({
            y:_heading_top.y + _heading_top.height, 
            x:config.width/2, 
            scene:this, 
            align:"center",
            text:"0", 
            color:"#3b792d",
            size:120
        });
        _number.setAlpha(0);

        let _heading_bottom = new PineText({
            y:_number.y + _number.height, scene:this, align:"center", text:" MEALS SO FAR THIS SEASON!", size:40, shadowSize:1,
            x:config.width/2
        });
        _heading_bottom.setAlpha(0);

        var _buttons = new WoodButtonGroup({
            scene:this,
            y: _heading_bottom.y + _heading_bottom.height + 100,
            buttons:[ 
                {
                    key:"share",
                    text:"SHARE"
                },
                {
                    key:"play_again",
                    text:"PLAY AGAIN"
                }
            ],
            onClick:_key=>{
                switch(_key){
                    case "share":
                    this.toShareMode();
                    break;

                    case "play_again":
                    window.parent.closesharewindow();
                    this.initScene( "StartScene", { level:0 } );
                    break;

                    default:
                    return null;
                }
            }
        });
        _buttons.x = (config.width/2) - (_buttons.getBounds().width/2);
        _buttons.setAlpha(0);

        this.heading_top = _heading_top;
        this.number = _number;
        this.heading_bottom = _heading_bottom;
        this.buttons = _buttons;

        this.add.existing( _heading_top, _number, _heading_bottom );
        this.add.image(config.width/2, config.height-100, "workbench");

        if( this.game.session.meals > 0 ){
            let _t = this;

            console.log( "Post Meals: ", this.game.session.user );

            $.ajax({
                method: "POST",
                url: "http://luckeyou.com/grab-a-meal/service.php",
                data:{
                    "avatar" : this.game.session.avatar,
                    "meals" : this.game.session.meals,
                    "level" : this.game.session.level,
                    "user" : this.game.session.user
                }
            }).done(function( total ) {                
                _t.transitionIn(total);
            });
        }
    }

    toShareMode(){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets: this.buttons.buttons[0],
            alpha:0,
            duration:200,
        });

        timeline.add({
            targets: this.buttons,
            x:"-=70",
            duration:200
        });

        timeline.add({
            targets: [this.heading_bottom,this.number,this.heading_top],
            alpha:0,
            duration:200,
            onComplete:e=>{
                window.parent.opensharewindow();
            }
        });

        timeline.play();
    }

    initScene( _name, _params ){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets: this.buttons,
            alpha:0,
            duration:200,
        });

        timeline.add({
            targets: this.heading_bottom,
            alpha:0,
            duration:200,
        });

        timeline.add({
            targets: this.number,
            alpha:0,
            duration:200
        });
        
        timeline.add({
            targets: this.heading_top,
            alpha:0,
            duration:200,
            onComplete:e=>{
                this.scene.start( _name, _params );
            }
        });

        timeline.play();
    }

    tally(_meals){
        let _score = {val:0};

        this.tweens.add({
            targets:_score,
            val:_meals,
            onStart:e=>{
                this.background_scene.playSound('effects-tally', {volume:.4});
            },
            onUpdate:e=>{
                this.number.text = Math.round(_score.val);
                this.number.x = (config.width/2) - (this.number.width/2);
            },
        })
    }

    transitionIn(total){
        this.meal_pool.init({
            spawn_rate: 400,
            max_size:6
        });

        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets:[this.heading_top],
            alpha:1,
            y: "+=50",
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.add({
            targets: this.number,
            alpha:1,
            y: "+=50",
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
            onComplete:e=>{
                this.tally(total);
            }
        });

        timeline.add({
            targets: this.heading_bottom,
            alpha:1,
            y: "+=50",
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.add({
            targets: this.buttons,
            alpha:1,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.play();
    }
}

export default DoneScene;