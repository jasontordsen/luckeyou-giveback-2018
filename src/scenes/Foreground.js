/* globals __DEV__ */
import Phaser from 'phaser'
import config from "../config";

class ForegroundScene extends Phaser.Scene {

    constructor(config) {
        super( {...config, 'key': 'ForegroundScene'} );
    }

    create(data) {
        this.light = this.add.image(40, config.height, "light");
        this.light.setAlpha(0);

        this.snowman = this.add.image(config.width - 70, config.height + 120, "snowman");
        this.snowman.setAlpha(0);

        this.snow = this.add.image(config.width/2, config.height+100, "snow");
        this.snow.setAlpha(0);

        var particles = this.add.particles('particles');

        particles.createEmitter({
            frames: ['flakes/1.png','flakes/2.png','flakes/3.png','flakes/4.png'],
            y: 0,
            x: { min: 0, max: 1024 },
            lifespan: 5000,
            speedY: { min: 50, max: 100 },
            scale: { start: 0.2, end: .1 },
            quantity: 1
        });

        this.transitionIn();
    }

    transitionIn(){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets: this.snow,
            alpha:1,
            y: config.height-20,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.add({
            targets:[this.light],
            alpha:1,
            y: config.height-240,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.add({
            targets: this.snowman,
            alpha:1,
            y: config.height-80 ,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.play();
    }
}

export default ForegroundScene;