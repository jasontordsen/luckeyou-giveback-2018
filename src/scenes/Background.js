/* globals __DEV__ */
import Phaser from 'phaser'
import TextButton from '../objects/TextButton'

class BackgroundScene extends Phaser.Scene {

    constructor(config) {
        super( {...config, 'key': 'BackgroundScene'} );
    }

    init(data){
        this.bg_audio = null;
    }

    create(data) {
        var particles = this.add.particles('particles');

        particles.createEmitter({
            frames: ['flakes/1.png'],
            y: 0,
            x: { min: 20, max: 1004 },
            lifespan: 1000,
            speedY: { min: 150, max: 300 },
            scale: { start: 0.5, end: .1 },
            quantity: .1
        });

        this.music_background = this.sound.add('music-background');

        this.effects_bonus = this.sound.add('effects-bonus');
        this.effects_grab = this.sound.add('effects-grab');
        this.effects_click = this.sound.add('effects-click');
        this.effects_clock = this.sound.add('effects-clock');
        this.effects_lose = this.sound.add('effects-lose');
        this.effects_miss = this.sound.add('effects-miss');
        this.effects_tally = this.sound.add('effects-tally');
        this.effects_woosh = this.sound.add('effects-woosh');
        this.effects_win_group = this.sound.add('effects-win-group');
        this.effects_lose_group = this.sound.add('effects-lose-group');
        this.effects_level_lose = this.sound.add('effects-level-lose');
        this.effects_level_win = this.sound.add('effects-level-win');
        this.effects_game_win = this.sound.add('effects-game-win');
    }

    playSound( _name, config = {volume:.1, rate:1}, _once ){
        
        const _sound = this[ _name.replace(new RegExp("-", 'g'),"_") ];

        if(!_once || !_sound.isPlaying){
            _sound.play( config );
        }
    }

    stopSound( _name ){
        const _sound = this[ _name.replace(new RegExp("-", 'g'),"_") ];

        if(_sound.isPlaying)
            _sound.stop();
    }

    stopAllSounds( ){
        this.sound.stopAll();
    }

    setSoundVol( _name, _vol ){
        const _sound = this[ _name.replace(new RegExp("-", 'g'),"_") ];

        if(_sound.isPlaying)
            _sound.setVolume(_vol)
    }

    setSoundRate( _name, _rate ){
        const _sound = this[ _name.replace(new RegExp("-", 'g'),"_") ];

        if( _sound.isPlaying )
            _sound.setRate( _rate )
    }
}

export default BackgroundScene;