/* globals __DEV__ */
import Phaser from 'phaser'
import PineText from '../objects/PineText'
import config from "../config";
import WoodButton from '../objects/WoodButton';
import MealPool from "../objects/MealPool";

class IntroScene extends Phaser.Scene {

  constructor( config ) {
    super( { ...config, 'key': 'IntroScene' } );
  }

  init(){
    this.background_scene = this.scene.get('BackgroundScene');
    this.background_scene.playSound( "music-background", { volume:.1, rate:1, loop:true } );
    this.matter.world.setBounds( -30, 0, config.width + 60, 470, 90, false, false, false, true );
  }
  
  create() {
    this.meal_pool = new MealPool({
      scene: this
    });
    
    this.heading_top = new PineText({
        y:-10, 
        x:config.width/2, 
        scene:this, 
        align:"center", 
        text:"GRAB A MEAL,",
        size:96
    });
    this.heading_top.setAlpha(0);

    this.heading_bottom = new PineText({
        x:config.width/2, 
        y:50, 
        scene:this, 
        color:"#3b792d", 
        align:"center", 
        text:"GIVE A MEAL!",
        size:96
    });
    this.heading_bottom.setAlpha(0);

    this.play_button = new WoodButton( { 
        scene: this, 
        x: config.width/2, 
        y: 50 + this.heading_top.height + this.heading_bottom.height, 
        text: "PLAY",
        key: 'play', 
        onClick: _key=>{ this.handleLabelClick( _key ) } 
    });
    this.play_button.setAlpha(0);

    this.bench = this.add.image(config.width/2, config.height+400, "workbench");

    this.transitionIn();
  }

  transitionIn(){
    

    var timeline = this.tweens.createTimeline();
      timeline.add({
        targets: this.bench,
        y:config.height-100,
        ease: Phaser.Math.Easing.Cubic.Out,
        duration:300,
        onStart:e=>{
          this.background_scene.playSound('effects-woosh', {volume:.2});
        }
      });

      timeline.add({
        targets:this.heading_top,
        alpha:1,
        y: 30,
        ease: Phaser.Math.Easing.Back.Out,
        duration:300,
        onStart:e=>{
          this.background_scene.playSound('effects-woosh', {volume:.2});
        }
      });

      timeline.add({
        targets: this.heading_bottom,
        alpha:1,
        y: 15 + this.heading_top.height ,
        ease: Phaser.Math.Easing.Back.Out,
        duration:300,
        onComplete:e=>{
          this.meal_pool.init({
            spawn_rate: 200,
            max_size:6
          });
        }
      });

      timeline.add({
        targets: this.play_button,
        alpha:1,
        scaleX:ismobile ? 1.4 :  1,
        scaleY:ismobile ? 1.4 :  1,
        ease: Phaser.Math.Easing.Back.Out,
        duration:300,
        delay:2300
      });

      timeline.play();
  }

  handleLabelClick( _key ){
    this.scene.start( 'StartScene' );
  }
}

export default IntroScene;