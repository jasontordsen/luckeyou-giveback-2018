/* globals __DEV__ */
import Phaser from 'phaser'
import FullGroup from '../objects/LuckeGroup'
import PineText from '../objects/PineText'
import config from "../config";
import WoodButton from '../objects/WoodButton';

class StartScene extends Phaser.Scene {

  constructor( config ) {
    super( { ...config, 'key': 'StartScene' } );
  }

  init(){
    this.game.session.reset();
    this.background_scene = this.scene.get('BackgroundScene');
  }
  
  create() {
    
    let _heading = new PineText({
      y:-50, x:config.width/2, scene:this, align:"center", text:"Choose your helper", size:70
    });
    _heading.alpha = 0;

    this.lucke_group = new FullGroup( { scene:this, start_anim:"attract", interactive:true, onClick:_key => {
      this.handleLabelClick(_key);
    }});

    let jason_label = new WoodButton( { scene:this,x:160,y:100,text:"JASON", key:'jason', onClick: _key=>{ this.handleLabelClick( _key ) } } );
    jason_label.alpha = 0;

    let tim_label = new WoodButton( { scene:this,x:390,y:100,text:"TIMMY", key:'tim', onClick: _key=>{ this.handleLabelClick( _key ) } } );
    tim_label.alpha = 0;

    let nick_label = new WoodButton( { scene:this,x:620,y:100,text:"NICK", key:'nick', onClick: _key=>{ this.handleLabelClick( _key ) } } );
    nick_label.alpha = 0;

    let manny_label = new WoodButton( { scene:this,x:850,y:100,text:"MANNY", key:'manny', onClick: _key=>{ this.handleLabelClick( _key ) } } );
    manny_label.alpha = 0;

    let _labels = [
      jason_label,
      tim_label,
      nick_label,
      manny_label
    ]

    this.tweens.add({
      targets:[_heading],
      y: 30,
      alpha:1,
      duration: 300,
      ease: "Cubic",
      delay: 2000,
    });

    this.tweens.add({
      targets:[nick_label,jason_label,tim_label,manny_label],
      y: 160,
      alpha:1,
      duration: 300,
      ease: "Cubic",
      delay: 1600,
    });

    this.add.image(config.width/2, config.height-100, "workbench");
  }

  handleLabelClick( _key ){
    this.lucke_group.stopAnims();

    this.game.session.avatar = _key;

    if(this.game.session.first_play){
      this.scene.start( 'InstructionsScene', { avatar:_key, level:config.game.start_level } );
      this.game.session.first_play = false;
    } else {
      this.scene.start( 'GameScene', { avatar:_key, level:config.game.start_level } );
    }
  }
}

export default StartScene;