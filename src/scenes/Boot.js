import Phaser from 'phaser'

class BootScene extends Phaser.Scene {

  constructor(config) {
    super( {...config, 'key': 'BootScene'} );
  }

  preload() {
    // this.scoreText = this.add.text(200, 20, 'Loading... ', { fill: '#000' });
    this.load.image('snow', 'assets/images/snow.png');
    this.load.image('light', 'assets/images/light.png');
    this.load.image('workbench', 'assets/images/workbench.png');
    this.load.image('snowman', 'assets/images/snowman.png');
    this.load.image('gift', 'assets/images/gift.png');
    this.load.image('game_bg', 'assets/images/game_bg.png');
    this.load.multiatlas('sprites', 'assets/sprites/sprites.json', 'assets/sprites');
    this.load.multiatlas('ui', 'assets/ui/ui.json', 'assets/ui');

    this.load.atlas('particles', 'assets/particles/particles.png', 'assets/particles/particles.json');

    //music
    this.load.audio('music-background', ['assets/audio/music/background.mp3'] );

    this.load.audio('effects-bonus', ['assets/audio/effects/bonus.mp3'] );
    this.load.audio('effects-tally', ['assets/audio/effects/tally.mp3'] );
    this.load.audio('effects-woosh', ['assets/audio/effects/woosh.mp3'] );
    this.load.audio('effects-grab', ['assets/audio/effects/grab.mp3'] );
    this.load.audio('effects-click', ['assets/audio/effects/click.mp3'] );
    this.load.audio('effects-clock', ['assets/audio/effects/clock.mp3'] );
    this.load.audio('effects-lose', ['assets/audio/effects/lose.mp3'] );
    this.load.audio('effects-lose-group', ['assets/audio/effects/lose-group.mp3'] );
    this.load.audio('effects-miss', ['assets/audio/effects/miss.mp3'] );
    this.load.audio('effects-win-group', ['assets/audio/effects/win-group.mp3'] );
    this.load.audio('effects-level-win', ['assets/audio/effects/level-win.mp3'] );
    this.load.audio('effects-level-lose', ['assets/audio/effects/level-lose.mp3'] );
    this.load.audio('effects-game-win', ['assets/audio/effects/game-win.mp3'] );
  }

  create(data) {
    this.scene.launch('BackgroundScene');
    this.scene.start('IntroScene', {level:0});
    this.scene.launch('ForegroundScene');
  }
}

export default BootScene;
