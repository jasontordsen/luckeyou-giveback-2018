/* globals __DEV__ */
import Phaser from 'phaser'
import WoodButtonGroup from '../objects/WoodButtonGroup';
import PineText from '../objects/PineText';
import DescriptionWithNumber from '../objects/DescriptionWithNumber';
import config from "../config";
import FullGroup from '../objects/LuckeGroup';
import Award from '../objects/Award';

class GameWinScene extends Phaser.Scene {

    constructor( config ) {

        super( { ...config, 'key': 'GameWinScene' } );
    }

    init( { avatar:_avatar, meals:_meals, level:_level = 0 } ){
        this.game.session.level = _level+1;
        this.background_scene = this.scene.get( 'BackgroundScene' );
    }

    create( { avatar:_avatar, meals:_meals, level:_level = 0 } ){

        this.heading = new PineText({
            y:-20, x:config.width/2, scene:this, align:"center", text:"YOU DID IT!", size:70
        });
        this.heading.setAlpha(0);

        this.description = new DescriptionWithNumber(
            {
                scene:this, 
                x:0,
                y:80,
                before_text:"You grabbed all ",
                after_text:" meals!",
                number: 100
            }
        );
        this.description.setAlpha(0);

        this.description_bounds = this.description.getBounds();

        this.description.x = (config.width/2)-this.description_bounds.width/2;

        this.buttons = new WoodButtonGroup({
            scene:this,
            x:0,
            y:150,
            buttons:[ 
                {
                    key:"play_again",
                    text:"PLAY AGAIN"
                },
                {
                    key:"done",
                    text:"I'M DONE"
                }
            ],
            onClick:_key=>{
                switch(_key){
                    case "play_again":
                    this.initScene( "StartScene", { level:0 } );
                    break;

                    case "done":
                    this.initScene( "DoneScene" );
                    break;

                    default:
                    return null;
                }
            }
        });
        this.buttons.setAlpha(0);

        this.buttons.x = (config.width/2) - (this.buttons.getBounds().width/2);

        this.lucke_group = new FullGroup( { scene:this, start_anim:"win-group", leader:_avatar } );

        this.add.image(config.width/2, config.height-100, "workbench");
        
        this.award = new Award({scene:this});

        // this.add.image(200,100,"ui","star.png");

        this.background_scene.playSound('effects-game-win', {volume:.2});

        this.transitionIn();
    }

    transitionIn(){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets:[this.heading],
            alpha:1,
            y: 30,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
            delay:800
        });
    
        timeline.add({
            targets: this.description,
            alpha:1,
            y: 38 + this.heading.height ,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
            delay:200
        });
    
        timeline.add({
            targets: this.buttons,
            alpha:1,
            y: 68 + this.heading.height + this.description_bounds.height,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
            delay:2800,
            onComplete:()=>{
                this.award.show(()=>{
                    this.addBonus();
                });
            }
        });
    
        timeline.play();
    }

    initScene( _name, _params ){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets:[this.heading],
            alpha:0,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });
    
        timeline.add({
            targets: this.description,
            alpha:0,
            ease: Phaser.Math.Easing.Back.Out,
            duration:200,
        });
    
        timeline.add({
            targets: this.buttons,
            alpha:0,
            ease: Phaser.Math.Easing.Back.Out,
            duration:100,
        });

        timeline.add({
            targets: this.award,
            alpha:0,
            ease: Phaser.Math.Easing.Back.Out,
            duration:50,
        });

        timeline.add({
            ...config.avatars.transitions.out,
            targets: this.lucke_group,
            onComplete:e=>{
                this.scene.start( _name, _params );
            }
        });

        timeline.play();
    }

    addBonus(){
        let _score = {val:100};

        this.tweens.add({
            targets:_score,
            val:200,
            delay:400,
            onStart:e=>{
                this.background_scene.playSound('effects-bonus', {volume:.4});
                this.background_scene.playSound('effects-tally', {volume:.4});
            },
            onUpdate:e=>{
                this.description.update( Math.round(_score.val) );
            },
        })
    }
}

export default GameWinScene;