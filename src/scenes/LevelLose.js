/* globals __DEV__ */
import Phaser from 'phaser'
import FullGroup from '../objects/LuckeGroup';
import config from "../config";
import WoodButtonGroup from '../objects/WoodButtonGroup';
import PineText from '../objects/PineText';
import DescriptionWithNumber from '../objects/DescriptionWithNumber';

class LevelLoseScene extends Phaser.Scene {

    constructor( config ) {

        super( { ...config, 'key': 'LevelLoseScene' } );
    }

    init( { avatar:_avatar, meals:_meals, level:_level = 0 } ){

        this.background_scene = this.scene.get( 'BackgroundScene' );
    }

    create( { avatar:_avatar, meals:_meals, level:_level = 0, conf } ){

        let _heading = new PineText({
            y:-40, x:config.width/2, scene:this, align:"center", text:"Good Try", size:70
        });
        _heading.setAlpha(0);

        let _description = new DescriptionWithNumber(
            {
                scene:this, 
                x:0,
                y:_heading.y + _heading.height + 8,
                before_text:"You grabbed ",
                after_text:" out of " + conf.goal + " meals",
                number:_meals
            }
        );
        _description.setAlpha(0);

        let _description_bounds = _description.getBounds();

        _description.x = (config.width/2)-_description_bounds.width/2;

        var _buttons_list = [
            {
                key:"try_again",
                text:"TRY AGAIN"
            }
        ]

        if( this.game.session.meals > 0 ){
            _buttons_list.push(
                {
                    key:"done",
                    text:"I'M DONE"
                }
            )
        }

        _buttons_list.push(
            {
                key:"start_over",
                text:"START OVER"
            }
        )
        
        this.lucke_group = new FullGroup( { scene:this, start_anim:"lose-group", leader:_avatar } );

        var _buttons = new WoodButtonGroup({
            scene:this,
            x:0,
            y:50 + _description.y + _description_bounds.height + (ismobile ? 45 : 28),
            buttons:_buttons_list,
            onClick:_key=>{
                switch(_key){
                    case "try_again":
                    this.initScene( "GameScene", { avatar:_avatar, level:( _level ) } );
                    break;

                    case "start_over":
                    this.initScene( "StartScene", { level:0 } );
                    break;

                    case "done":
                    this.initScene( "DoneScene" );
                    break;

                    default:
                    return null;
                }
            }
        });
        _buttons.setAlpha(0);

        this.heading = _heading;
        this.description = _description;
        this.buttons = _buttons;

        _buttons.x = (config.width/2) - (_buttons.getBounds().width/2);

        this.add.image(config.width/2, config.height-100, "workbench");

        this.background_scene.playSound('effects-level-lose', {volume:.2});

        this.transitionIn();
    }

    transitionIn(){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets: this.heading,
            y:"+=50",
            alpha:1,
            ease: Phaser.Math.Easing.Back.Out,
            duration:300,
        });

        timeline.add({
            targets: this.description,
            y:"+=50",
            alpha:1,
            ease: Phaser.Math.Easing.Back.Out,
            delay:100,
            duration:400,
        });
        
        timeline.add({
            targets: this.buttons,
            alpha:1,
            ease: Phaser.Math.Easing.Back.Out,
            delay:100,
            duration:400,
        });

        timeline.play();
    }

    initScene( _name, _params ){
        var timeline = this.tweens.createTimeline();

        timeline.add({
            targets: this.buttons,
            alpha:0,
            duration:200,
        });

        timeline.add({
            targets: this.description,
            alpha:0,
            duration:200
        });
        
        timeline.add({
            targets: this.heading,
            alpha:0,
            duration:200,
        });

        timeline.play();

        this.tweens.add({
            ...config.avatars.transitions.out,
            targets: this.lucke_group,
            onComplete:e=>{
                this.scene.start( _name, _params );
            }
        });
    }
}

export default LevelLoseScene;