/* globals __DEV__ */
import Phaser from "phaser";
import Avatar from "../sprites/Avatar";
import config from "../config";
import MealPool from "../objects/MealPool";
import ProgressBar from "../objects/ProgressBar";
import LevelInfo from "../objects/LevelInfo";
import Score from "../objects/Score";

class GameScene extends Phaser.Scene {
  constructor(config) {
    super({ ...config, key: "GameScene" });
  }

  init({ avatar: _avatar, level: _level = 0 }) {
    this.gameover = null;
    this.fdown = false;
    this.meals = 0;
    this.level = _level;
    this.misses = 0;
    this.deaths = 0;
    this.level_conf = config.game.levels[ _level ];

    this.background_scene = this.scene.get("BackgroundScene");
    this.background_scene.setSoundRate( "music-background", 1+(_level*.05) );
    this.background_scene.setSoundVol( "music-background", .1 );

    this.cursors = this.input.keyboard.createCursorKeys();

    this.meal_pool = new MealPool({
      scene: this,
      onMiss: this.handleDeath.bind(this)
    });

    this.input.keyboard.on("keyup", e => {
      if(!this.gameover){
        if (e.code == "Space") {
          if (this.meal_pool.checkHit(this.avatar.x)) {
            this.onPoint();
          } else {
            this.avatar.playAnim("angry");
            this.handleMiss();
          }
        }
      }
    });

    this.matter.world.setBounds( -30, 0, config.width + 60, 470, 90, false, false, false, true );

    this.mealsText = null;

    if( !$("body").hasClass("game-scene" ) ){
      $("body").addClass("game-scene");
    }
  }

  handleDeath() {

    if(!this.gameover){
      this.deaths++;

      if (this.game_timer != undefined)
        this.game_timer.timeScale = this.level_conf.death_factor;
    }
  }

  handleMiss() {
    if(!this.gameover){
      this.misses++;

      if (this.game_timer != undefined)
        this.game_timer.timeScale = this.level_conf.miss_factor;
    }
  }

  create({ avatar: _avatar, level:_level = 0, from_instructions:_from_instructions = false }) {

    $("#touch-space").off().click(function(){
      if (this.meal_pool.checkHit( this.avatar.x, 80)) {
        this.onPoint();
      } else {
        this.avatar.playAnim("angry");
        this.handleMiss();
      }
    }.bind(this) );

    this.score = new Score({scene:this,x:(ismobile? 200 : 120),y:50,text:"0",key:"score"});

    this.progress_bar = new ProgressBar(
      {
        scene:this, 
        x: config.width - (config.width/4) - (ismobile ? 170 : 80),
        y:50,
        width:config.width / 2,
        level:_level+1
      }
    );

    this.avatar = new Avatar({
      key: _avatar,
      scene: this,
      x: config.width/2, y: 810,
      animations: config.avatars.animations[_avatar],
      onAnimComplete: this.handleAvatarAnimComplete.bind(this)
    });

    this.level_info = new LevelInfo( {conf:this.level_conf, scene:this, x:config.width/2, y:20, onStart:e=>{
      this.startGame();
    }});
    this.level_info.setAlpha(0);

    this.add.image(config.width/2, config.height-100, "workbench");

    this.transitionIn( _from_instructions );
  }

  handleAvatarAnimComplete(_key) {
    if( _key.indexOf("angry") != -1 ) {
    }
  }

  transitionIn(_from_instructions) {
    this.level_info.show();

    this.tweens.add({
      ...config.avatars.transitions.in,
      targets: this.avatar,
      onComplete:e=>{
        this.avatar.playAnim("idle");

        this.time.addEvent({
          delay: 800,
          callback: e => {
            this.startGame();
          },
          callbackScope: this
        });
      }
    });
  }

  startGame() {
    this.progress_bar.show();
    this.score.show();
    this.level_info.hide();
    this.startTimer(this.level);
    this.meal_pool.init({
      level: this.level,
      lifespan: this.level_conf.lifespan,
      spawn_rate: this.level_conf.spawn_rate,
      max_size:this.level_conf.max_pool_size
    });
  }

  startTimer() {
    this.game_timer = this.time.addEvent({
      timeScale: 1,
      delay: this.level_conf.duration,
      callback: e => {
        this.initGameOver();
      },
      callbackScope: this
    });
  }

  initGameOver( _win ){ 
    this.progress_bar.stop();

    $("body").removeClass("game-scene");

    $("#touch-space").off();
    this.background_scene.setSoundVol( "music-background", .03 );
    this.background_scene.setSoundRate( "music-background", 1 );

    let _score = Math.round((this.level_conf.duration - this.game_timer.elapsed)/10);
    _score -= this.misses*10;
    _score -= this.deaths*100; 
    _score = Phaser.Math.Clamp( _score, 0, 100000 );
    
    this.gameover = true;
    this.game_timer.destroy(); 
    this.game_timer = null;
    this.meal_pool.destroy();

    const _won = _win || this.meals >= this.level_conf.goal;

    this.tweens.add({
      ...config.avatars.transitions.out,
      targets: this.avatar,
      onComplete:e=>{
        this.onGameOver(_won, _score);
      }
    });
  }

  onGameOver( _won, _score ) {
    this.avatar.stopAnims();
    

    if(_won){
      this.meals = this.level_conf.goal;

      if( this.level == config.game.levels.length-1 ){
        this.scene.start("GameWinScene", {
          avatar: this.avatar.key,
          meals: this.meals,
          level: this.level,
          score:_score,
          conf:this.level_conf
        });
      } else {
        this.scene.start("LevelWinScene", {
          avatar: this.avatar.key,
          meals: this.meals,
          level: this.level,
          score:_score,
          conf:this.level_conf
        });
      }

      this.game.session.score += _score;
      this.game.session.meals += this.meals;
    } else {
      this.scene.start("LevelLoseScene", {
        avatar: this.avatar.key,
        meals: this.meals,
        level: this.level,
        conf:this.level_conf
      });
    }
  }

  update() {

    if(!this.gameover){

      this.meal_pool.update();

      if (this.game_timer) {
        const _progress = this.game_timer.getOverallProgress();

        if(_progress > .7){
          this.progress_bar.warn();
        }

        this.progress_bar.update(_progress);
      }

      if (this.cursors.left.isDown || touch_left_down === true) {
        const _newx = Phaser.Math.Clamp(
          this.avatar.x - this.level_conf.avatar_speed,
          config.game.bounds[0],
          config.game.bounds[1]
        );

        this.avatar_tween = this.tweens.add({
          targets: this.avatar,
          x: _newx,
          ease: "Sine",
          duration: 100
        });
      } else if (this.cursors.right.isDown || touch_right_down === true) {
        const _newx = Phaser.Math.Clamp(
          this.avatar.x + this.level_conf.avatar_speed,
          config.game.bounds[0],
          config.game.bounds[1]
        );

        this.tweens.add({
          targets: this.avatar,
          x: _newx,
          ease: "Sine",
          duration: 100
        });
      } else {
        if(this.game_timer) this.tweens.killTweensOf(this.avatar);
      }
    }
  }

  onPoint() {

    if(!this.gameover){

      if(this.score){
        this.tweens.killTweensOf(this.score);
        this.score.y = 50;
      }

      this.background_scene.playSound( "effects-grab" );

      if( this.game_timer ) this.game_timer.timeScale = 1;

      this.meals++;

      this.avatar.playAnim("gift");

      this.score.update(this.meals);

      if( this.meals >= this.level_conf.goal ){

        this.initGameOver( true );
      } else {

        this.tweens.add({
          targets: this.score,
          y: 20,
          yoyo: true,
          ease: Phaser.Math.Easing.Back.In,
          duration: 300
        });
      }
    }
  }
}

export default GameScene;
