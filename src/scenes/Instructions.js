/* globals __DEV__ */
import Phaser from 'phaser'
import PineText from '../objects/PineText'
import config from "../config";
import WoodButton from '../objects/WoodButton';
import Avatar from '../sprites/Avatar';
import DescriptionWithNumber from '../objects/DescriptionWithNumber';

class InstructionsScene extends Phaser.Scene {

  constructor( config ) {
    super( { ...config, 'key': 'InstructionsScene' } );
  }

  init({ avatar: _avatar, level:_level = 0 }){
    this.background_scene = this.scene.get('BackgroundScene');
  }
  
  create({ avatar: _avatar, level:_level = 0 }) {
    this.heading_top = new PineText({
        y:0, 
        x:config.width/2, 
        scene:this, 
        align:"center", 
        text:"USE           TO MOVE AND", 
        size:70
    });
    this.heading_top.setAlpha(0);

    this.heading_bottom = new PineText({
        x:config.width/2, 
        y:this.heading_top.y + this.heading_top.height - 4, 
        scene:this, 
        color:"#3b792d", align:"center", text:(ismobile ? "       " : "                    ") + "TO GRAB A MEAL", 
        size:70
    });
    this.heading_bottom.setAlpha(0);

    this.description = new DescriptionWithNumber(
      {
          scene:this, 
          x:0,
          y:this.heading_bottom.y + this.heading_bottom.height + 15,
          before_text:"Complete all 10 levels for a ",
          after_text:" meal bonus",
          number:100
      }
    );
    this.description.setAlpha(0);

    this.description_bounds = this.description.getBounds();

    this.description.x = (config.width/2)-this.description_bounds.width/2;

    this.play_button = new WoodButton( { 
        scene: this, 
        x: config.width/2, 
        y: 100 + this.heading_top.height + this.heading_bottom.height + this.description_bounds.height, 
        text: "GOT IT",
        key: 'play', 
        onClick: _key=>{ this.handleLabelClick( _key, _avatar, _level ) } 
    });
    this.play_button.setAlpha(0);
    this.play_button.setScale(.5);

    this.arrows  = this.add.image((config.width/2)-90,this.heading_top.y+39,"ui","arrows.png");
    this.arrows.setAlpha(0);

    this.space  = this.add.image((config.width/2)-160,this.heading_bottom.y+39,"ui", ismobile ? "space-mobile.png" : "space.png" );
    this.space.setAlpha(0);

    this.add.image(config.width/2, config.height-100, "workbench");

    this.transitionIn();
  }

  transitionIn(){
    if( !$("body").hasClass("game-scene" ) ){
      $("body").addClass("game-scene");
    }
    
    var timeline = this.tweens.createTimeline();

    timeline.add({
      targets: [this.heading_top,this.arrows],
      y:"+=50",
      alpha:1,
      ease: Phaser.Math.Easing.Back.Out,
      duration:300,
    });

    timeline.add({
      targets: [this.heading_bottom,this.space],
      y:"+=50",
      alpha:1,
      ease: Phaser.Math.Easing.Back.Out,
      delay:100,
      duration:400,
      onStart:e=>{
        this.background_scene.playSound('effects-woosh', {volume:.2, rate:1});
      }
    });
    
    timeline.add({
      targets: this.description,
      y:"+=50",
      alpha:1,
      ease: Phaser.Math.Easing.Back.Out,
      delay:100,
      duration:400,
    });

    timeline.add({
      targets: this.play_button,
      alpha:1,
      scaleX:ismobile ? 1.4 :  1,
      scaleY:ismobile ? 1.4 :  1,
      ease: Phaser.Math.Easing.Back.Out,
      delay:100,
      duration:400,
    });

    timeline.play();
  }

  handleLabelClick( _key, _avatar, _level ){
    this.scene.start( 'GameScene', {avatar:_avatar, level:_level, from_instructions:true} );
  }
}

export default InstructionsScene;